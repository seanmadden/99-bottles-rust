pub mod beer_song {
    pub fn verse(n: i32) -> String {
        String::from(match n {
            0 => { "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n" }
            1 => { "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n" }
            2 => { "2 bottles of beer on the wall, 2 bottles of beer.\nTake one down and pass it around, 1 bottle of beer on the wall.\n" }
            3..=99 => { return format!("{} bottles of beer on the wall, {} bottles of beer.\nTake one down and pass it around, {} bottles of beer on the wall.\n", n, n, n - 1); }
            _ => { "" }
        })
    }

    pub fn sing(start: i32, end: i32) -> String {
        (end..=start).rev()
            .map(|verse_num| verse(verse_num))
            .collect::<Vec<String>>()
            .join("\n")
    }
}